/**
 * Para inicializar el WebDriver
 */

const selenium = require('selenium-webdriver');
const capabilities = selenium.Capabilities.chrome();
capabilities.set('chromeOptions',{
    'args': ['--no-sandbox', 'start-maximized' , '--disable-gpu'],
    "w3c": false
})
capabilities.setPageLoadStrategy('normal');
const driver = new selenium.Builder().withCapabilities(capabilities).forBrowser("chrome").build();  
exports.driver = driver; 