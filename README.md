1. Validación del formulario y registro de un usuario: Se han realizado algunas validaciones del formulario
en el feature Test1/Test1Definition. No se ha realizado el flujo para un ingreso de un usuario ya que
no quería utilizar ruts al azar para hacer una inscripcion real.

2. Inicio de Sesion al sitio:  Creado bajo el Test2/Test2Definition

3. Validar precio de venta y total: Creado bajo el Test2/Test2Definition

4. Se han automatizado 5 pruebas para la API bajo el Test3/Test3Definition


Para ejecutar las pruebas se peude hacer desde los tags de cada una de ellas o desde los Tags de los features
ej: 
    Para ejecutar una prueba: ./node_modules/.bin/cucumber-js --tags "@test1_API"
    Para ejecutar un feature: ./node_modules/.bin/cucumber-js --tags "@Test_API"
    Para ejecutar todas las pruebas: ./node_modules/.bin/cucumber-js --tags "@regresion"

