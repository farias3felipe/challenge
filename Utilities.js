const selenium = require('selenium-webdriver'),

/**
 * Para detener la ejecucion X segundos esperando la carga del DOM
 */
function sleep(segs)
{
    console.log('Espero ' + segs);
    var e = new Date().getTime() + (segs * 1000);
    while (new Date().getTime() <= e) {}
}

/**
 * Convierte un dataTable a un array
 */
async function dataTableAlist(dataTable)
{
    let data = await dataTable.raw();
    let result = new Array();
    for(var i = 0; i < data[0].length; i++)
    {
        result.push(data[0][i]);
    }
    return result;
}

/**
 * Ingresa un texto letra por letra.
 */
async function ingresarTexto(webelement, texto)
{
    let cadena = texto.split("");
    for(var i=0; i<cadena.length; i++)
    {
        sleep(1)
        await webelement.sendKeys(cadena[i]);
    }
         
}

/**
 * Quita los simbolos de un numero
 */
async function limpiarNumero(num)
{
    num = num.replace("$ ","") 
    num = num.replace(".","")
    num = num.replace(",","")
    return parseInt(num);
}

module.exports = {sleep, dataTableAlist, ingresarTexto, limpiarNumero}