@Test_API @regresion
Feature: Validacion API

    @test1_API
    Scenario: Test - Validar respuesta con codigo 400
        Then valido que el codigo de respuesta es "200" para el endpoint "https://catfact.ninja/fact?max_length=20"

    @test2_API
    Scenario: Test - Validar respuesta con codigo 400
        Then valido que el codigo de respuesta es "404" para el endpoint "https://catfact.ninja/null"

    @test3_API
    Scenario: Test - Validar que el atributo fact no sea null o blanco
        Then valido que el atributo "fact" es distinto de null para el endpoint "https://catfact.ninja/fact?max_length=20"

    @test4_API
    Scenario: Test - Validar que el atributo length no sea null o blanco
        Then valido que el atributo "length" es distinto de null para el endpoint "https://catfact.ninja/fact?max_length=20"

    @test5_API
    Scenario: Test - Validar que el json de respuesta no contenga datos nulos
        Then valido que endpoint "https://catfact.ninja/breeds?limit=1" responde los datos requeridos
            | breed | country | origin | coat | pattern |