
    const { Given, When, Then, AfterAll} = require('@cucumber/cucumber');
    var assert = require('assert');
    var {setDefaultTimeout} = require('@cucumber/cucumber');
    setDefaultTimeout(60 * 1000);
    const selenium = require('selenium-webdriver'),
    By = selenium.By;
    const driver = require('../../Base.js').driver; 
    const utilidades = require('../../Utilities'); 

    
    When('ingreso como user {string} y pass {string}',async function (email, pass) 
    {
        utilidades.sleep(3)
        emailWE = await driver.findElement(By.name('email')); 
        await utilidades.ingresarTexto(emailWE, email)
        utilidades.sleep(5)
        passWE = await driver.findElement(By.name('password')); 
        await utilidades.ingresarTexto(passWE, pass)
    });

    When('presiono el boton {string}',async function (btn) 
    {
        utilidades.sleep(5);
        await driver.findElement(By.xpath("//span[contains(text(),'"+btn+"')]")).click();  
    });

    Then('valido que se muestre el nombre de usuario {string}',async function (userOK) 
    {
        utilidades.sleep(3);
        assert((await driver.findElement(By.xpath("//p[contains(text(),'"+userOK+"')]"))).isDisplayed, 
            "No se encontró el nombre del user en la vista")
    });


    When('cierro los modales informativos',async function () 
    {
        utilidades.sleep(3);
        await driver.findElement(By.xpath("//p[contains(text(),'En otro momento')]//parent::div")).click(); 
    });

    Given('presiono el boton para agregar al carrito',async function () {
        utilidades.sleep(3);
        list = await driver.findElement(By.xpath("//*[contains(text(),'Agregar al Carro')]//parent::div//parent::button")).click();
    });

    When('presiono el boton para ir al carro',async function () {
        utilidades.sleep(3);
        await driver.findElement(By.xpath("//a[contains(text(),'Ir al Carro')]")).click();    
    });

    Then('valido que el precio del producto coincide con el total del carro',async function () 
    {
        precio = await driver.findElement(By.xpath("//span//parent::div[@datatestid='price-1']")).getText(); 
        precio = await utilidades.limpiarNumero(precio);
        
        precioTotal = await driver.findElement(By.xpath("//span[@data-testid='total-amount']")).getText(); 
        precioTotal = await utilidades.limpiarNumero(precioTotal);  
        assert(precio == precioTotal, 
            "Los valores no coinciden, el producto indica <<"+precio+">> y el total <<"+precioTotal+">>")
    });
