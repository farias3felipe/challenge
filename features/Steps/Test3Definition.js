
    const { Given, When, Then} = require('@cucumber/cucumber');
    var assert = require('assert');
    var XMLHttpRequest = require('xhr2');
    const utilidades = require('../../Utilities'); 


    Then('valido que el codigo de respuesta es {string} para el endpoint {string}',function (codigo, endpoint) {
        const request = new XMLHttpRequest();
        request.open('GET', endpoint, true);
        request.onload = function () 
        {
            if(request.status != codigo)
                assert(false, "El codigo no es el correcto, se esperaba <<"+codigo+">> y ha respondido: "+request.status); 
        }
        request.send();   
    });


    Then('valido que el atributo {string} es distinto de null para el endpoint {string}', function (atributo, endpoint) {
        const request = new XMLHttpRequest();
        request.open('GET', endpoint, true);
        request.onload = function () 
        {
            const data =  JSON.parse(this.response);
            if(data[atributo].length == 0 || data[atributo] === null)
                assert(false, "El atributo es null o esta en blanco"); 
        }
        request.send();   
    });

    Then('valido que endpoint {string} responde los datos requeridos',async function (endpoint, dataTable) {
        const request = new XMLHttpRequest();
        request.open('GET', endpoint, true);
        request.onload = async function () 
        {
            const data =  JSON.parse(this.response);
            let campos = await utilidades.dataTableAlist(dataTable);
            for(var i = 0; i<data["data"].length; i++) //recorre los subnodos de "data"
            {
                campos.forEach(async function(e)
                {
                    console.log(data["data"][i][e]) 
                    if(data["data"][i][e] === null) //Valida solo que los atributos pasados en el datatable no sean null en el JSON de respuesta
                            assert(false, "El atributo <<"+e+">> es null");
                })    
            }
        }
        request.send();   
      });
  