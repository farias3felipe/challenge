   const { Given, When, Then, AfterAll} = require('@cucumber/cucumber');
   var assert = require('assert');
   var {setDefaultTimeout} = require('@cucumber/cucumber');
   setDefaultTimeout(60 * 1000);
   const selenium = require('selenium-webdriver'),
   By = selenium.By;
   Keys = selenium.Key;
   const driver = require('../../Base.js').driver; 
   const utilidades = require('../../Utilities'); 

   
   Given('me dirigo a {string}', async function (web) {
      await driver.get(web);
      utilidades.sleep(3);
   });

   When('cierro la ventana emergente', async function () {
      utilidades.sleep(3);
      if((await driver.findElements(By.xpath("//div[@class='dy-lb-close']"))).length>0){
         await driver.findElement(By.xpath("//div[@class='dy-lb-close']")).click();
      }   
   });

   When('ingreso en inicia sesion', async function () {
      utilidades.sleep(3);
      await driver.findElement(By.id('testId-UserAction-userinfo')).click();
   });

   When('ingreso en {string}', async function (submenu) 
   {
      utilidades.sleep(4);
      await driver.findElement(By.xpath("//a[contains(text(),'"+submenu+"')]")).click();
   });

   Then('valido que se despliegue el formulario de inicio de sesion', async function () 
   {
      utilidades.sleep(4);
      assert((await driver.findElements(By.xpath("//h1[contains(text(),'Regístrate')]"))).length>0, "El elemento no se ha desplegado")
   });

   Then('valido que existan los campos requeridos en los CA', async function (dataTable) 
   {
      let campos = await utilidades.dataTableAlist(dataTable);
      campos.forEach(async function(e)
      {
         assert((await driver.findElement(By.xpath("//span//span[contains(@class,'copy3 primary') and contains(text(),'"+e+"')]"))).isDisplayed, 
            "El campo <<"+e+">> no se ha desplegado en la vista.")
      })
   });

   When('ingreso como email del registro {string}',async function (mail) 
   {
      await driver.findElement(By.name('email')).sendKeys(mail, Keys.TAB);
      utilidades.sleep(4);
   });

   Then('valido que se mueestre el mensaje de error por mail no valido',async function () 
   {
      assert((await driver.findElement(By.xpath("//span[contains(text(),'Ingresa un correo electrónico')]"))).isDisplayed, 
         "El mensaje <<Ingresa un correo electrónico>> no se ha desplegado en la vista.")
   });

   AfterAll(async function()
   {
      await driver.quit();
   });

   