@Test_InicioVenta @regresion
Feature: Validacion de inicio de sesion y carro de compras

    @test1_IV
    Scenario: Test - Validar se permite iniciar sesion con una cuenta dada
        Given me dirigo a "https://www.falabella.com/falabella-cl"
        And cierro la ventana emergente
        When ingreso en inicia sesion
        And ingreso en "Inicia sesión"
        And ingreso como user "test@test2.cl" y pass "Test2021"
        And presiono el boton "Ingresar"
        And cierro los modales informativos
        Then valido que se muestre el nombre de usuario "Felipe"

    @test2_IV
    Scenario: Test - Validar que al agregar un producto al carro de comprar el total es correcto
        Given me dirigo a "https://www.falabella.com/falabella-cl/product/7200532/Celular-Adulto-Mayor-Blu-Joy-Boton-Sos-3G-Dual-Sim/7200532"
        And presiono el boton para agregar al carrito
        When presiono el boton para ir al carro
        Then valido que el precio del producto coincide con el total del carro
