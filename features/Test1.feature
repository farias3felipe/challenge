@Test_Formulario @regresion
Feature: Validacion del Formulario de  registro

    @test1_TF
    Scenario: Test - Validar que se despliega la vista para el registro de un cliente
        Given me dirigo a "https://www.falabella.com/falabella-cl"
        And cierro la ventana emergente
        When ingreso en inicia sesion
        And ingreso en "Regístrate"
        Then valido que se despliegue el formulario de inicio de sesion

    @test2_TF
    Scenario: Test - Validar que la vista de registro tenga todos los campos requeridos
        Given me dirigo a "https://www.falabella.com/falabella-cl"
        And cierro la ventana emergente
        When ingreso en inicia sesion
        And ingreso en "Regístrate"
        Then valido que existan los campos requeridos en los CA
            | Nombre | Primer Apellido | RUT | Celular | Correo Electrónico | Contraseña |

    @test3_TF
    Scenario Outline: Test - Validar que el campo mail solicita el formato correcto
        Given me dirigo a "https://www.falabella.com/falabella-cl"
        And cierro la ventana emergente
        When ingreso en inicia sesion
        And ingreso en "Regístrate"
        And ingreso como email del registro <mail>
        Then valido que se mueestre el mensaje de error por mail no valido

        Examples:
            | mail           |
            | "test.test.cl" |
            | "@test.cl"     |
